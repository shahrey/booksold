﻿using BookSold.Data;
using BookSold.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookSold.Services
{
   public class AuctionsService
    {
        public void SaveAuction (Auction auction)
        {
            BookSoldContext context = new BookSoldContext();

            context.Auctions.Add(auction);

            context.SaveChanges();
        }

        public Auction GetAuctionByID (int ID) //for Auction Edit
        {
            BookSoldContext context = new BookSoldContext();

            return context.Auctions.Find(ID);
        }

        public void UpdateAuction(Auction auction) // For Auction that we have added so update to do
        {
            BookSoldContext context = new BookSoldContext();

            context.Entry(auction).State = System.Data.Entity.EntityState.Modified;

            context.SaveChanges();
        }

        public List<Auction> GetAllAuctions() //for Auction Edit
        {
            BookSoldContext context = new BookSoldContext();

            return context.Auctions.ToList();
        }

        public List<Auction> GetPromotedAuctions() //for Auction Edit
        {
            BookSoldContext context = new BookSoldContext();

            return context.Auctions.Take(4).ToList();
        }

        public void DeleteAuction(Auction auction) // For Auction that we have added so update to do
        {
            BookSoldContext context = new BookSoldContext();

            context.Entry(auction).State = System.Data.Entity.EntityState.Deleted;

            context.SaveChanges();
        }

    }
}
