﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookSold.Web.ViewModels
{
    public class PageViewModels
    {
        public string PageTitle { get; set; }
        public string PageDescription { get; set; }

    }
}