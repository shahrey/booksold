﻿using BookSold.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BookSold.Web.ViewModels
{
    public class AuctionsViewModel:PageViewModels
    {
        public List<Auction> AllAuctions { get; set; }
        public List<Auction> PromotedAuctions { get; set; }
    }
}