﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BookSold.Web.Startup))]
namespace BookSold.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
