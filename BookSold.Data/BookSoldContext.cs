﻿using BookSold.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookSold.Data
{
    public class BookSoldContext : DbContext
    {
        public BookSoldContext() : base("name=BookSoldConstrng")
        {
        }

        public DbSet<Auction> Auctions { get; set; }


    }
}
